from django import forms
from .models import User, Game_list, Rating, Comment, VisualNovel, Voice_actor, Genre, Company, Character
from django.contrib.auth.backends import BaseBackend
from django.db.models import Q
import random

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
    
    class Meta:
        model = User
        fields  = ('login_name', 'email')
        
    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password1'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']
    
    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.special_key = random.randint(1000, 9999)
        if(commit):
            user.save()
        return user

class UserLoginForm(forms.Form):
    login_name = forms.CharField(max_length=200, label='Username')
    password = forms.CharField(widget=forms.PasswordInput, label='Password')

    def clean(self):
        cleaned_data = super().clean()
        login_name = cleaned_data.get('login_name')
        password = cleaned_data.get('password')

        try:
            user = User.objects.get(login_name=login_name)
        except User.DoesNotExist:
            raise forms.ValidationError('Invalid username.')

        if not user.check_password(password):
            raise forms.ValidationError('Invalid password.')

        cleaned_data['user'] = user
        return cleaned_data

class GameListForm(forms.ModelForm):
    STATUS_CHOICE = (
        (0, 'Not Playing'),
        (1, 'Playing'),
        (2, 'Planning'),
        (3, 'Finished'),
    )
    status = forms.ChoiceField( choices=STATUS_CHOICE, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Game_list
        fields = ('status', 'vn_id')
        widgets = {
            'vn_id': forms.HiddenInput(),
        }
        
    def __init__(self, *args, **kwargs):
        super(GameListForm, self).__init__(*args, **kwargs)
        self.fields['vn_id'].widget = forms.HiddenInput()
        
    def save(self, commit=True):
        instance = super().save(commit=False)
        try:
            game_list = Game_list.objects.get(user_id=instance.user_id, vn_id=instance.vn_id)
            game_list.status = instance.status
            instance = game_list
        except Game_list.DoesNotExist:
            pass
        if commit:
            instance.save()
        return instance

class RatingForm(forms.ModelForm):
    RATE_CHOICES = (
        (0, '0 - Not rated'),
        (1, '1 - Terrible'),
        (2, '2 - Very bad'),
        (3, '3 - Bad'),
        (4, '4 - Poor'),
        (5, '5 - Average'),
        (6, '6 - Fine'),
        (7, '7 - Good'),
        (8, '8 - Very good'),
        (9, '9 - Great'),
        (10, '10 - Masterpiece'),
    )
    rate = forms.ChoiceField(choices=RATE_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Rating
        fields = ('rate', 'vn_id')
        widgets = {
            'vn_id': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(RatingForm, self).__init__(*args, **kwargs)
        self.fields['vn_id'].widget = forms.HiddenInput()

    def save(self, commit=True):
        instance = super().save(commit=False)
        try:
            rating = Rating.objects.get(user_id=instance.user_id, vn_id=instance.vn_id)
            rating.rate = instance.rate
            instance = rating
        except Rating.DoesNotExist:
            pass
        if commit:
            instance.save()
        return instance

SEARCH_CHOICES = (
    ('vn', 'Visual Novels'),
    ('co', 'Companies'),
    ('va', 'Voice Actors'),
    ('ch', 'Characters'),
    ('ge', 'Genres'),
)

class SearchForm(forms.Form):
    search_type = forms.ChoiceField(choices=SEARCH_CHOICES)
    query = forms.CharField(max_length=100, required=True)

    def search(self):
        cleaned_data = self.cleaned_data
        search_type = cleaned_data.get('search_type')
        query = cleaned_data.get('query')

        if search_type == 'vn':
            return VisualNovel.objects.filter(title__icontains=query)
        elif search_type == 'co':
            return Company.objects.filter(name__icontains=query)
        elif search_type == 'va':
            return Voice_actor.objects.filter(Q(first_name__icontains=query) | Q(last_name__icontains=query))
        elif search_type == 'ch':
            return Character.objects.filter(Q(first_name__icontains=query) | Q(last_name__icontains=query))
        elif search_type == 'ge':
            return Genre.objects.filter(name__icontains=query)

class CommentForm(forms.ModelForm):
    
    class Meta:
        model = Comment
        fields = ('comment', 'vn_id', 'user_id')
        widgets = {
            'vn_id': forms.HiddenInput(),
            'user_id': forms.HiddenInput(),
        }
        
    def save(self, commit=True):
        instance = super().save(commit=False)
        if commit:
            instance.save()
        return instance
        
class VisualNovelForm(forms.ModelForm):
    class Meta:
        model = VisualNovel
        fields = ['title', 'released', 'description', 'image', 'company', 'genres']
        widgets = {
            'released': forms.TextInput(attrs={'type': 'date'}),
            'genres': forms.CheckboxSelectMultiple(),
        }



















class UserBackend(BaseBackend):
    def authenticate(self, request, login_name=None, password=None):
        try:
            user = User.objects.get(login_name=login_name)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
