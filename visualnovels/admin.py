from django.contrib import admin
from .models import (
    VisualNovel, Company, Character, Genre, VisualNovelGenre,
    Language, System_language, Voice_language, Nationality,
    Voice_actor, Character_voice_actor, Character_genre, User,
    Character_liked, Comment, Game_list, Rating,
)

admin.site.register(VisualNovel)
admin.site.register(Company)
admin.site.register(Character)
admin.site.register(Genre)
admin.site.register(VisualNovelGenre)
admin.site.register(Language)
admin.site.register(System_language)
admin.site.register(Voice_language)
admin.site.register(Nationality)
admin.site.register(Voice_actor)
admin.site.register(Character_voice_actor)
admin.site.register(Character_genre)
admin.site.register(User)
admin.site.register(Character_liked)
admin.site.register(Comment)
admin.site.register(Game_list)
admin.site.register(Rating)
