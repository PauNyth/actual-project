from django.db import models
from django.contrib.auth.hashers import make_password, check_password
# Create your models here.


class Company(models.Model):
    company_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    description = models.TextField(null=False, default='No description')
    def __str__(self):
        return self.name
    
    def visualnovel_set(self):
        return self.visualnovel_set.all()

    def get_class_name(self):
        return self.__class__.__name__.lower()

class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    login_name = models.CharField(max_length=200, unique=True)
    email = models.EmailField()
    password = models.CharField(max_length=200)
    special_key = models.IntegerField(null=True)
    description = models.TextField(null=False, default='No description')
    last_login = models.DateTimeField(auto_now=True)

    
    def set_password(self, raw_password):
        self.password = make_password(raw_password)
        
    def check_password(self, raw_password):
        return check_password(raw_password, self.password)
    
    def __str__(self):
        return self.login_name

class VisualNovel(models.Model):
    vn_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    released = models.DateField()
    description = models.TextField(null=False, default='No description')
    image = models.ImageField(upload_to='images/')
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    genres = models.ManyToManyField('Genre', through='VisualNovelGenre')
    status = models.ManyToManyField('User', through='Game_list')
    
    def __str__(self):
        return self.title
    
    def genre_set(self):
        return self.genres.all()
    def company_get(self):
        return self.company.name
    def status_get(self, user: User):
        try:
            game_list = Game_list.objects.get(user_id=user, vn_id=self)
            return game_list.status
        except Game_list.DoesNotExist:
            return None
        
    def get_class_name(self):
        return 'visual_novel'

class Character(models.Model):
    character_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    age = models.IntegerField()
    birth_date = models.DateField()
    description = models.TextField(null=False, default='No description')
    company_id = models.ForeignKey(Company, on_delete=models.DO_NOTHING)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.DO_NOTHING)
    name: str = first_name.__str__() + " " + last_name.__str__()
    def __str__(self):
        return self.first_name + " " + self.last_name
    
    def get_class_name(self):
        return self.__class__.__name__.lower()

class Genre(models.Model):
    genre_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    description = models.TextField(null=False, default='No description')
    
    def __str__(self):
        return self.name
    
    def visualnovel_set(self):
        try:
            visualnovels = VisualNovelGenre.objects.filter(genre_id=self)
            return visualnovels
        except VisualNovelGenre.DoesNotExist:
            return None
    def get_class_name(self):
        return self.__class__.__name__.lower()

class VisualNovelGenre(models.Model):
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE, related_name='visualnovelgenre')
    genre_id = models.ForeignKey(Genre, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('vn_id', 'genre_id'),)
        
    def __str__(self):
        return f"{self.vn_id.title} - {self.genre_id.name}" 

class Language(models.Model):
    language_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name

class System_language(models.Model):
    language_id = models.ForeignKey(Language, on_delete=models.CASCADE)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('language_id', 'vn_id'),)

class Voice_language(models.Model):
    language_id = models.ForeignKey(Language, on_delete=models.CASCADE)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('language_id', 'vn_id'),)

class Nationality(models.Model):
    nationality_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name

class Voice_actor(models.Model):
    va_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    age = models.IntegerField()
    birth_date = models.DateField()
    nationality_id  = models.ForeignKey(Nationality, on_delete=models.DO_NOTHING)
    language_id = models.ForeignKey(Language, on_delete=models.DO_NOTHING)
    name: str = first_name.__str__() + " " + last_name.__str__()
    
    def __str__(self):
        return self.first_name + " " + self.last_name
    
    def get_characters(self):
        try:
            characters = Character_voice_actor.objects.filter(va_id=self)
            return characters
        except Character_voice_actor.DoesNotExist:
            return None
        
    def get_class_name(self):
        return self.__class__.__name__.lower()
    
class Character_voice_actor(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    va_id = models.ForeignKey(Voice_actor, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('character_id', 'va_id'),)

class Character_genre(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    genre_id = models.ForeignKey(Genre, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('character_id', 'genre_id'),)

class Character_liked(models.Model):
    character_id = models.ForeignKey(Character, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('character_id', 'user_id'),)

class Comment(models.Model):
    comment_id = models.AutoField(primary_key=True)
    comment = models.TextField()
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE)

class Game_list(models.Model):
    status = models.IntegerField(default=0)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE)
    
    class Meta:
        unique_together = (('user_id', 'vn_id'),)

class Rating(models.Model):
    rate = models.IntegerField(default=0)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    vn_id = models.ForeignKey(VisualNovel, on_delete=models.CASCADE)
    
    RATE_CHOICES = (
        (0, '0 - Not rated'),
        (1, '1 - Terrible'),
        (2, '2 - Very bad'),
        (3, '3 - Bad'),
        (4, '4 - Poor'),
        (5, '5 - Average'),
        (6, '6 - Fine'),
        (7, '7 - Good'),
        (8, '8 - Very good'),
        (9, '9 - Great'),
        (10, '10 - Masterpiece'),
    )
    
    def __str__(self):
        return self.rate

