from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('home/', views.home, name='home'),
    path('visualnovels/detail/<int:vn_id>/', views.visual_novel_detail, name='visual_novel_detail'),
    path('logout/', views.logout, name='logout'),
    path('visualnovels/', views.visual_novels, name='visual_novels'),
    path('companies/', views.company_list, name='company_list'),
    path('companies/detail/<int:company_id>/', views.company_detail, name='company_detail'),
    path('characters/', views.characters, name='characters'),
    path('characters/detail/<int:character_id>/', views.character_detail, name='character_detail'),
    path('genres/', views.genres, name='genres'),
    path('genres/detail/<int:genre_id>/', views.genre_detail, name='genre_detail'),
    path('voice_actors/', views.voice_actors_list, name='voice_actors_list'),
    path('voice_actors/detail/<int:va_id>/', views.voice_actor_detail, name='voice_actor_detail'),
    path('add_visualnovel/', views.add_visualnovel, name='add_visualnovel'),
]