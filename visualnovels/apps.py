from django.apps import AppConfig


class VisualnovelsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'visualnovels'
