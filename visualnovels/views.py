from django.shortcuts import render, get_object_or_404, redirect
from .forms import UserCreationForm, UserLoginForm, GameListForm, RatingForm, CommentForm, VisualNovelForm, SearchForm
from django.contrib.auth import authenticate

from .models import VisualNovel, Character, User, Game_list, Company, Genre, Voice_actor, Rating, Comment
from django.db.models import Count, Avg, Q
# Create your views here.


def index(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['login_name']
            password = form.cleaned_data['password']
            user = authenticate(request, login_name=username, password=password)
            
            if user is not None:
                request.session['user'] = user.user_id
                return redirect('home')
            else:
                form.add_error(None, 'Invalid login')
    else:
        form = UserLoginForm()
    context = {'form': form}
    return render(request, 'visualnovels/index.html', context=context)

def logout(request):
    del request.session['user']
    return redirect('index')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            request.session['user'] = user.user_id
            return redirect('index')
    else:
        form = UserCreationForm()
    return render(request, 'visualnovels/register.html', {'form': form})

def home(request):
    user = User.objects.get(user_id=request.session['user'])
    form = SearchForm(request.GET or None)
    results = []

    if request.GET and form.is_valid():
        results = form.search()

    context = {
        'form': form,
        'results': results,
        'user': user,
    }

    return render(request, 'visualnovels/home.html', context=context)

def visual_novel_detail(request, vn_id):
    vn = get_object_or_404(VisualNovel, vn_id=vn_id)
    user = User.objects.get(user_id=request.session['user'])
    game_list, created = Game_list.objects.get_or_create(user_id=user, vn_id=vn)
    rating, rating_created = Rating.objects.get_or_create(user_id=user, vn_id=vn)
    comment_form = CommentForm(request.POST or None, initial={'vn_id': vn_id, 'user_id': user.user_id})
    comments = Comment.objects.filter(vn_id=vn_id)
    if request.method == 'POST':
        if 'comment_form' in request.POST and comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.user_id = user
            comment.vn_id = vn
            comment.save()
            return redirect('visual_novel_detail', vn_id=vn_id) 
        else:
            rating_form = RatingForm(request.POST, instance=rating)
            list_form = GameListForm(request.POST, instance=game_list)
            if rating_form.is_valid() and list_form.is_valid():
                rating_form.save()
                list_form.save()
                return redirect('visual_novel_detail', vn_id=vn_id)
    else:
        rating_form = RatingForm(instance=rating)
        list_form = GameListForm(instance=game_list)
        if rating is None:
            rating_form.fields['rate'].choices = [('choose', 'Choose')] + rating_form.fields['rate'].choices
        if game_list is None:
            list_form.fields['status'].choices = [('choose', 'Choose')] + list_form.fields['status'].choices

    ratings = Rating.objects.filter(vn_id=vn_id)
    genres = vn.genre_set()
    characters = Character.objects.filter(vn_id=vn_id)
    rating_stats = ratings.aggregate(Avg('rate'), Count('rate'))
    rating_counts = [ratings.filter(rate=choice[0]).count() for choice in Rating.RATE_CHOICES]
    
    context = {'vn': vn, 'genres': genres, 'characters': characters, 'user': user, 'ratings': ratings, 'game_list': game_list, 'rating_form': rating_form, 'list_form': list_form, 'comment_form': comment_form, 'comments': comments, 'rating_stats': rating_stats, 'rating_counts': rating_counts}
    return render(request, 'visualnovels/visual_novel_detail.html', context=context)

def visual_novels(request):

    vn_list = VisualNovel.objects.all()
    user = User.objects.get(user_id=request.session['user'])
    status_list = []
    for vn in vn_list:
        status = vn.status_get(user.user_id)
        status_list.append(status)
    super_list = zip(vn_list, status_list)
    context = {'super_list': super_list, 'user': user}
    return render(request, 'visualnovels/visual_novels.html', context=context)

def company_list(request):
    companies = Company.objects.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'companies': companies, 'user': user}
    return render(request, 'visualnovels/company_list.html', context=context)

def company_detail(request, company_id):
    company = get_object_or_404(Company, company_id=company_id)
    vn_list = company.visualnovel_set.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'company': company, 'vn_list': vn_list, 'user': user}
    return render(request, 'visualnovels/company_detail.html', context=context)

def character_detail(request, character_id):
    character = get_object_or_404(Character, character_id=character_id)
    user = User.objects.get(user_id=request.session['user'])
    vn = character.vn_id
    context = {
        'character': character,
        'vn': vn,
        'user': user,
    }
    return render(request, 'visualnovels/character_detail.html', context)

def characters(request):
    characters = Character.objects.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'characters': characters, 'user': user}
    return render(request, 'visualnovels/characters.html', context=context)

def genres(request):
    genres = Genre.objects.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'genres': genres, 'user': user}
    return render(request, 'visualnovels/genres.html', context=context)

def genre_detail(request, genre_id):
    genre = get_object_or_404(Genre, genre_id=genre_id)
    vn_list = genre.visualnovel_set.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'genre': genre, 'vn_list': vn_list, 'user': user}
    return render(request, 'visualnovels/genre_detail.html', context=context)

def voice_actors_list(request):
    voice_actors_list = Voice_actor.objects.all()
    user = User.objects.get(user_id=request.session['user'])
    context = {'voice_actors_list': voice_actors_list, 'user': user}
    return render(request, 'visualnovels/voice_actors_list.html', context=context)

def voice_actor_detail(request, va_id):
    voice_actor = get_object_or_404(Voice_actor, va_id=va_id)
    characters = voice_actor.get_characters()
    user = User.objects.get(user_id=request.session['user'])
    context = {'voice_actor': voice_actor, 'characters': characters, 'user': user}
    return render(request, 'visualnovels/voice_actor_detail.html', context=context)

def add_visualnovel(request):
    if request.method == 'POST':
        form = VisualNovelForm(request.POST, request.FILES)
        if form.is_valid():
            novel = form.save(commit=False)
            novel.save()
            form.save_m2m()
            return redirect('visual_novel_detail', vn_id=novel.vn_id)
    else:
        form = VisualNovelForm()
    return render(request, 'visualnovels/add_visual_novel.html', {'form': form})